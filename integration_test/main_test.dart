import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:hetsaldo/main.dart' as app;

Future<void> pumpUntilFound(
  WidgetTester tester,
  Finder finder, {
  Duration timeout = const Duration(seconds: 10),
}) async {
  bool timerDone = false;
  final timer = Timer(timeout, () => timerDone = true);
  while (timerDone != true) {
    await tester.pumpAndSettle();

    final found = tester.any(finder);
    if (found) {
      timerDone = true;
    }
  }
  timer.cancel();
}

void main() {
  final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  setUpAll(() async {
    WidgetsApp.debugAllowBannerOverride = false;
    if (Platform.isAndroid) {
      await binding.convertFlutterSurfaceToImage();
    }
  });

  group('end-to-end test', () {
    testWidgets('render home and add ticket', (tester) async {
      app.main();
      await tester.pumpAndSettle(const Duration(seconds: 10)); // delays are for slow iOS simulator on a poor old Optiplex
      expect(find.byTooltip('Add Card'), findsOneWidget);

      await binding.takeScreenshot('0-home-empty');

      final Finder addButton = find.byTooltip('Add Card');
      await tester.tap(addButton);
      await tester.pumpAndSettle();
      expect(find.text('Ticket number'), findsOneWidget);
      final Finder addField = find.ancestor(
        of: find.text('Ticket number'),
        matching: find.byType(TextField),
      );
      // await tester.tap(addField);
      await tester.enterText(addField, "D0021B68C0D1A2F9");
      await tester.pumpAndSettle(const Duration(seconds: 5));
      await binding.takeScreenshot('1-add-ticket');
      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pump();

      await pumpUntilFound(tester, find.byTooltip('Show More'));
      await tester.pumpAndSettle(const Duration(seconds: 5));
      expect(find.text('D002 1B68 C0D1 A2F9'), findsWidgets);

      await binding.takeScreenshot('0-home-ticket');
      await tester.pumpAndSettle();
      final Finder moreButton = find.byTooltip('Show More');
      await tester.tap(moreButton);
      await tester.pumpAndSettle(const Duration(seconds: 5));

      await binding.takeScreenshot('0-home-ticket-expand');
    });
  });
}
