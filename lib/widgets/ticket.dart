import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ionicons/ionicons.dart';

import 'package:hetsaldo/widgets/ticket_details.dart';
import '../data/card.dart';

class TicketWidget extends StatefulWidget {
  final SmartTicket ticket;

  const TicketWidget({super.key, required this.ticket});

  @override
  State<TicketWidget> createState() => _TicketWidgetState();
}

class _TicketWidgetState extends State<TicketWidget>
    with SingleTickerProviderStateMixin {
  SmartTicket get ticket => widget.ticket;
  bool _expanded = false;

  late AnimationController _animationController;
  late Animation<double> _animation;
  final Duration _animationDuration = const Duration(milliseconds: 300);

  bool _isOnTrip() {
    if (ticket.smartTicketLines == null || ticket.smartTicketLines!.isEmpty) {
      return false;
    }
    DateTime? lastTripStartTime;
    for (var i = 0; i < ticket.smartTicketLines!.length; i++) {
      if (ticket.smartTicketLines![i].timestamp == null) {
        continue;
      }
      if (ticket.smartTicketLines![i].isOverstap ?? false) {
        continue;
      }
      lastTripStartTime =
          DateTime.tryParse(ticket.smartTicketLines![i].timestamp!);
    }

    // if last trip is less than 1 hours ago, the ticket is still valid and _onTrip is true
    if (lastTripStartTime != null &&
        DateTime.now().difference(lastTripStartTime).inHours < 1) {
      return true;
    }
    return false;
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: _animationDuration,
    );
    _animation = CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Flexible(
                  child: ListTile(
                    leading: const Icon(Ionicons.ticket),
                    title: Text(ticket.prettyID()),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ticket.isNotActive
                            ? Text(AppLocalizations.of(context)!.statusNotUsed)
                            : Text(AppLocalizations.of(context)!
                                .status(ticket.getStatusLanguage(context))),
                        _isOnTrip()
                            ? Padding(
                                padding: const EdgeInsets.only(top: 2),
                                child: Chip(
                                  avatar:
                                      const Icon(Icons.directions_bus_filled),
                                  padding: const EdgeInsets.all(3),
                                  label: Text(
                                      AppLocalizations.of(context)!.onTrip),
                                ),
                              )
                            : const SizedBox.shrink(),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: ticket.isNotActive
                        ? []
                        : [
                            Text(
                              ticket.value!.toString(),
                              style: const TextStyle(
                                  fontSize: 32.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              AppLocalizations.of(context)!.tripsLeftShort,
                              style: const TextStyle(
                                  fontSize: 12.0, color: Colors.grey),
                              textAlign: TextAlign.center,
                            ),
                          ],
                  ),
                ),
              ],
            ),
            AnimatedSize(
              duration: _animationDuration,
              child: SizeTransition(
                sizeFactor: _animation,
                child: TicketDetails(
                  ticket: ticket,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: _expanded
                      ? const Icon(Icons.arrow_circle_up)
                      : const Icon(Icons.arrow_circle_down),
                  tooltip: AppLocalizations.of(context)!.showMore,
                  onPressed: () {
                    setState(() {
                      _expanded = !_expanded;
                      if (_expanded) {
                        _animationController.forward();
                      } else {
                        _animationController.reverse();
                      }
                    });
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
