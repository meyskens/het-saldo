import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;

part 'card.g.dart';

@HiveType(typeId: 1)
class SmartTicket {
  @HiveField(0)
  String? id;

  @HiveField(2)
  int? oldTimestamp; // DEPRECATED

  @HiveField(3)
  String? validationStatus;

  @HiveField(4)
  int? value;

  @HiveField(5)
  List<SmartTicketLine>? smartTicketLines;

  @HiveField(6)
  bool? _isNotActive = false;

  @HiveField(7)
  String? timestamp;

  get isNotActive => value == null || _isNotActive == true;

  SmartTicket({this.id});

  SmartTicket.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['validations'] != null) {
      smartTicketLines = <SmartTicketLine>[];
      json['validations'].forEach((v) {
        smartTicketLines!.add(SmartTicketLine.fromJson(v));
      });
      for (int i = 0; i < smartTicketLines!.length; i++) {
        if (i > 0) {
          smartTicketLines![i].isOverstap =
              smartTicketLines![i].value == smartTicketLines![i - 1].value;
        }
      }
    }
    timestamp = json['timestamp'];
    validationStatus = json['validationStatus'];
    value = json['value'];
  }

  refreshData() async {
    final response = await http
        .get(Uri.parse('https://www.delijn.be/api/saldo-checker/$id/'));
    if (response.statusCode == 404) {
      // doing some heuristics to see if this could be a valid ticket
      // if it is we mark it as non active (yet)
      if (id?.length == 16 && BigInt.tryParse(id!, radix: 16) != null) {
        _isNotActive = true;
        return;
      }
      throw Exception('Ticket not found');
    }
    if (response.statusCode != 200) {
      throw Exception('Failed to load ticket');
    }
    // If the server did return a 200 OK response,
    // then parse the JSON.
    final ticket = SmartTicket.fromJson(jsonDecode(response.body));

    smartTicketLines = ticket.smartTicketLines;
    validationStatus = ticket.validationStatus;
    value = ticket.value;
    timestamp = ticket.timestamp;
    if (value != null) {
      _isNotActive = false;
    }
  }

  String prettyID() {
    if (id == null) return "";

    final buffer = StringBuffer();
    for (int i = 0; i < id!.length; i++) {
      buffer.write(id![i]);
      final nonZeroIndex = i + 1;
      if (nonZeroIndex % 4 == 0 && nonZeroIndex != id!.length) {
        buffer.write(' ');
      }
    }

    return buffer.toString();
  }

  String getStatusLanguage(BuildContext context) {
    // this is a reverse engineered list of possible statusses we saw
    switch (validationStatus) {
      case "VALID":
        return AppLocalizations.of(context)!.valid;
      case "NO_SALDO":
        return AppLocalizations.of(context)!.noSaldo;
    }

    return validationStatus ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    if (smartTicketLines != null) {
      data['validations'] = smartTicketLines!.map((v) => v.toJson()).toList();
    }
    data['timestamp'] = timestamp;
    data['validationStatus'] = validationStatus;
    data['value'] = value;
    return data;
  }
}

@HiveType(typeId: 2)
class SmartTicketLine {
  @HiveField(0)
  int? oldTimestamp;

  @HiveField(1)
  int? value;

  @HiveField(2)
  String? timestamp;

  @HiveField(3)
  bool? isOverstap = false;

  SmartTicketLine({this.timestamp, this.value});

  SmartTicketLine.fromJson(Map<String, dynamic> json) {
    timestamp = json['timestamp'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['timestamp'] = timestamp;
    data['value'] = value;
    return data;
  }
}

class CardIdCleaner {
  static String cleanID(String id) {
    id = id.toUpperCase();

    id = id.replaceAll(" ", "");
    id = id.replaceAll("O", "0");

    return id;
  }
}
