// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SmartTicketAdapter extends TypeAdapter<SmartTicket> {
  @override
  final int typeId = 1;

  @override
  SmartTicket read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SmartTicket(
      id: fields[0] as String?,
    )
      ..oldTimestamp = fields[2] as int?
      ..validationStatus = fields[3] as String?
      ..value = fields[4] as int?
      ..smartTicketLines = (fields[5] as List?)?.cast<SmartTicketLine>()
      .._isNotActive = fields[6] as bool?
      ..timestamp = fields[7] as String?;
  }

  @override
  void write(BinaryWriter writer, SmartTicket obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.oldTimestamp)
      ..writeByte(3)
      ..write(obj.validationStatus)
      ..writeByte(4)
      ..write(obj.value)
      ..writeByte(5)
      ..write(obj.smartTicketLines)
      ..writeByte(6)
      ..write(obj._isNotActive)
      ..writeByte(7)
      ..write(obj.timestamp);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SmartTicketAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class SmartTicketLineAdapter extends TypeAdapter<SmartTicketLine> {
  @override
  final int typeId = 2;

  @override
  SmartTicketLine read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SmartTicketLine(
      timestamp: fields[2] as String?,
      value: fields[1] as int?,
    )..oldTimestamp = fields[0] as int?;
  }

  @override
  void write(BinaryWriter writer, SmartTicketLine obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.oldTimestamp)
      ..writeByte(1)
      ..write(obj.value)
      ..writeByte(2)
      ..write(obj.timestamp);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SmartTicketLineAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
